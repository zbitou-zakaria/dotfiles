execute pathogen#infect()
syntax on
filetype plugin indent on
set tabstop=8 softtabstop=0 expandtab shiftwidth=2 smarttab
let g:syntastic_cpp_check_header = 1
let g:syntastic_cpp_include_dirs = ['/home/zakaria/projects/robot-features/include/', '/home/zakaria/projects/prototype-template-lib-robot-features/src/include/', '/home/zakaria/projects/prototype-template-lib-robot-features/tests/']
let g:syntastic_cpp_compiler_options = ' -std=c++11'
let g:syntastic_check_on_wq = 0
set colorcolumn=80

set t_Co=256                        " force vim to use 256 colors
let g:solarized_termcolors=256      " use solarized 256 fallback

syntax enable
set background=dark
colorscheme solarized

set encoding=utf-8
set scrolloff=3
set autoindent
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set visualbell
set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
set relativenumber
set undofile

nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
nnoremap <leader><space> :noh<cr>
nnoremap <tab> %
vnoremap <tab> %

set wrap
set textwidth=78
set formatoptions=qrn1

inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>

nnoremap ; :

let mapleader = ","
nnoremap <leader>v <C-w>v<C-w>l
nnoremap <leader>s <C-w>s<C-w>j

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
nnoremap <esc> :noh<return><esc>

let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes':   [],'passive_filetypes': [] }
noremap <leader>e :SyntasticCheck<CR>
noremap <leader>f :SyntasticToggleMode<CR>

let g:syntastic_quiet_messages={'level':'warnings'}

set textwidth=0
set wrap

let g:ycm_global_ycm_extra_conf = "~/.ycm_extra_conf.py"
let g:ycm_collect_identifiers_from_tags_files = 0
